'use strict';

var cluster = require('cluster');
var os = require('os');

/**
 * Cluster setup.
 */
cluster.setupMaster({
  exec: 'server.js'
});

cluster.on('exit', function (worker) {
  console.log('Worker ' + worker.id + ' died');
  // Replace the dead worker
  cluster.fork();
});

for (var i = 0; i < os.cpus().length; i++) {
  cluster.fork();
}
