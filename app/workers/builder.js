/**
 * Common scenario for setting up
 * @ndaidong at Twitter
 **/
'use strict';

var fs = require('fs');
var exec = require('child_process').execSync;

var bella = require('bellajs');
var mkdirp = require('mkdirp').sync;
var cpdir = require('copy-dir').sync;

var pkg = require('../../package'),
  bconf = pkg.builder || {},
  jsDir = fixPath(bconf.jsDir),
  imgDir = fixPath(bconf.imgDir),
  fontDir = fixPath(bconf.fontDir),
  distDir = fixPath(bconf.distDir);


function download(src, saveas, callback){

  var cb = callback || function(){};

  if(fs.existsSync(saveas)){
    fs.unlink(saveas);
  }
  console.log('Downloading '+src+'...');
  var cmd = 'wget -O '+saveas+' '+src;
  exec(cmd);
  console.log('Downloaded '+saveas);
  cb();
}

function fixPath(p){
  return p?(p+=p.charAt(p.length-1)==='/'?'':'/'):p;
}

var getFiles = function(){
  var files = bconf.files || {};
  if(bella.isObject(files)){
    var destDir = jsDir;
    var rd = fixPath(destDir);
    for(var alias in files){
      var src = files[alias];
      var dest = rd+alias+'.js';
      if(!fs.existsSync(dest)){
        download(src, dest);
      }
    }
  }
}

var publish = function(from, to){
  if(fs.existsSync(to)){
    exec('rm -rf '+to);
  }
  mkdirp(to);
  cpdir(from, to);
}

function setup(){
  if(!fs.existsSync(jsDir)){
    mkdirp(jsDir);
  }
  getFiles();

  publish(imgDir, distDir+'images/');
  publish(fontDir, distDir+'font/');
}

var builder = {
  setup: setup
}
module.exports = builder;
