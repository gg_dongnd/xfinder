'use strict';

var bella = require('bellajs');
var People = require('../models/People');

exports.index = function(req, res){

  var config = Object.create(res.config);
  var meta = config.application;

  var title = meta.title;

  var data = {
    meta: meta,
    title: title,
    people: {},
    coworkers: []
  }

  var query = req.query || {};
  var q = query.q || '';
  if(q){
    q = bella.decode(q);
  }

  var paths = (function(p){
    var ps = [];
    if(!!p && p.length>0){
      var _ps = p.charAt(0)=='/'?p.slice(1):p;
      if(!!_ps && _ps.length>0){
        ps = _ps.split('/');
      }
    }
    return ps;
  })(req.path || '');

  if(paths.length===2){
    q = paths[1];
  }

  if(q){
    var com;
    var done = function(){
      data.q = q;
      //res.json(data);
      res.render('people', data);
    }

    People.search(q).then(function(p){
      data.people = p;
      if(p.company.id){
        com = p.company;
      }
    }).finally(function(){
      if(com){
        People.findCoworkers(com.id).then(function(entries){
          var ls = entries.map(People.clean);
          var id = data.people.id;
          data.coworkers = ls.filter(function(item){
            return item.id!=id;
          });
        }).finally(done);
      }
      else{
        done();
      }
    });
  }
  else{
    res.redirect('/');
  }
}
