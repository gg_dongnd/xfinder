'use strict';

var bella = require('bellajs');
var URL = require('url');

function isDomain(s) {
  var reg = /^([\w]+\.[\w]+)+/ig;
  return bella.isString(s) && s.length > 4 && s.length < 2083 && reg.test(s);
}

exports.index = function(req, res){

  var meta = Object.create(res.config.application);

  var title = meta.title;

  var data = {
    meta: meta,
    title: title
  }

  var query = req.query || {};
  var q = query.q || '';
  if(q){
    q = bella.decode(q);

    if(bella.isEmail(q)){
      return res.redirect('/people?q='+bella.encode(q));
    }
    else if(isDomain(q)){
      return res.redirect('/companies?q='+bella.encode(q));
    }
    else{
      var url = URL.parse(q);
      if(url.hostname){
        return res.redirect('/companies?q='+bella.encode(url.hostname));
      }
    }
  }
  res.render('home', data);
}
