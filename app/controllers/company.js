'use strict';

var bella = require('bellajs');
var Company = require('../models/Company');
var People = require('../models/People');

var cleanCompany = function(c){
  return {
    id: c.id,
    name: c.name || '',
    domain: c.domain || ''
  }
}

exports.index = function(req, res){

  var config = Object.create(res.config);
  var meta = config.application;

  var title = meta.title;

  var data = {
    meta: meta,
    title: title,
    company: {},
    employees: []
  }

  var query = req.query || {};
  var q = query.q || '';
  if(q){
    q = bella.decode(q);
  }

  var paths = (function(p){
    var ps = [];
    if(!!p && p.length>0){
      var _ps = p.charAt(0)=='/'?p.slice(1):p;
      if(!!_ps && _ps.length>0){
        ps = _ps.split('/');
      }
    }
    return ps;
  })(req.path || '');

  if(paths.length===2){
    q = paths[1];
  }

  if(q){
    var com = false;
    var done = function(){
      data.q = q;
      //res.json(data);
      res.render('company', data);
    }

    Company.search(q).then(function(c){
      com = c;
      data.company = cleanCompany(com);
    }).finally(function(){
      if(com){
        People.findCoworkers(com.id).then(function(entries){
          data.employees = entries.map(People.clean);
        }).finally(done);
      }
      else{
        done();
      }
    });
  }
  else{
    res.redirect('/');
  }
}
