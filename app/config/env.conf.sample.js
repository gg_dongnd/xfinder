'use strict';

var domain = 'techpush.me';

module.exports = {
  application: {
    domain: domain,
    image: 'http://'+domain+'/images/brand/techpush.png',
    url: 'http://'+domain
  },
  mongodb: {
    url: 'mongodb://127.0.0.1:27017/techpush'
  }
}
