'use strict';

var pkg = require('../../package.json');

var config = {};

config.name = pkg.name || '';
config.version = pkg.version || '';
config.description = pkg.description || '';
config.keywords = pkg.keywords || '';
config.author = pkg.author || '';

config.baseDir = '/';

config.application = {
  name: 'xFinder',
  alias: 'xfinder',
  slogan: '',
  description: '',
  keywords: '',
  image: '',
  author: '',
  title: 'xFinder',
  domain: '',
  url: '',
  canonical: ''
}

config.port = 8887;

config.clearbitKey = 'af4e311f9c0bc45ee23d19e1d1de2ca5';

config.settings = {
  dateformat: 'D, M d, Y H:i:s O'
}

module.exports = config;
