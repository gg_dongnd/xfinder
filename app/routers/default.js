'use strict';

var homeController = require('../controllers/home');
var peopleController = require('../controllers/people');
var companyController = require('../controllers/company');

module.exports = function(app){
  app.get('/', homeController.index);

  app.get('/people/[a-z0-9A-Z]{32}', peopleController.index);
  app.get('/people', peopleController.index);

  app.get('/companies/[a-z0-9A-Z]{32}', companyController.index);
  app.get('/companies/', companyController.index);
}
