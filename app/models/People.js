'use strict';

var async = require('async');
var bella = require('bellajs');
var Promise = require('bluebird');

var config = require('../config/base');
var clearbit = require('clearbit')(config.clearbitKey);

var People = require('./schemas/People');
var Company = require('./Company');

function modify(item){
  var ob = {
    id: bella.createId(32),
    email: item.email,
    name: item.name.fullName || '',
    gender: item.gender || '',
    location: item.location || '',
    bio: item.bio || '',
    site: item.site || '',
    avatar: item.avatar
  }

  var em = item.employment || {};
  if(em.name && em.domain){
    var com = {
      id: bella.md5(em.domain),
      name: em.name,
      domain: em.domain
    }
    Company.push(com);
    ob.company = com;
  }
  var ga = item.gavatar || {};
  if(ga.avatar){
    ob.gavatar = ga.avatar;
  }

  var getNetwork = function(k){
    if(item[k].handle){
      ob[k] = item[k].handle;
    }
  }
  
  'twitter,linkedin,github,googleplus,facebook,angellist'.split(',').map(getNetwork)

  ob.saveAt = bella.time();

  People.findOne({email: item.email}, function(err, doc){
    if(!doc){
      var p = new People(ob);
      p.save(function(err){
        if(err){
          console.trace(err);
        }
      });
    }
  });

  return ob;
}

var searchWithClearbit = function(email){
  return new Promise(function(resolve, reject){
    clearbit.Person.find({email: email, stream: true})
    .then(function (person){
      var p = modify(person);
      resolve(p);
    }).catch(function(e){
      reject(e);
    });
  });  
}

var searchWithMongo = function(v){
  return new Promise(function(resolve, reject){
    People.findOne({$or: [{email: v}, {id: v}]}, function(e, doc){
      if(!e && doc){
        resolve(doc);
      }
      else{
        reject(e)
      }
    })
  }); 
}

People.search = function(email){
  var p = false;
  return new Promise(function(resolve, reject){
    searchWithMongo(email).then(function(res){
      resolve(res);
    }).catch(function(){
      resolve(searchWithClearbit(email));
    });
  });
}

People.findCoworkers = function(id){
  return new Promise(function(resolve, reject){
    People.find({'company.id': id}, function(e, docs){
      if(!e && docs){
        resolve(docs);
      }
      else{
        reject(e)
      }
    })
  }); 
}

var cleanPeople = function(p){
  return {
    id: p.id,
    name: p.name || '',
    email: p.email || '',
    avatar: p.avatar || ''
  }
}

People.clean = cleanPeople;

module.exports = People;
