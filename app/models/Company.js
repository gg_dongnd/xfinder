'use strict';

var async = require('async');
var bella = require('bellajs');
var Promise = require('bluebird');

var config = require('../config/base');
var clearbit = require('clearbit')(config.clearbitKey);

var Company = require('./schemas/Company');

function push(item){
  var ob = {
    id: bella.md5(item.domain),
    name: item.name || '',
    domain: item.domain || '',
    saveAt: bella.time()
  }
  
  Company.findOne({domain: item.domain}, function(err, doc){
    if(!doc){
      var c = new Company(ob);
      c.save(function(err){
        if(err){
          console.trace(err);
        }
      });
    }
  });

  return ob;
}

var searchWithClearbit = function(domain){
  var cc = clearbit.Company;
  return new Promise(function(resolve, reject){
    cc.find({domain: domain, stream: true})
    .then(function(com){
      var p = push(com);
      resolve(p);
    }).catch(function(e){
      reject(e);
    });
  });
}

var searchWithMongo = function(v){
  return new Promise(function(resolve, reject){
    Company.findOne({$or: [{domain: v}, {id: v}]}, function(e, doc){
      if(!e && doc){
        resolve(doc);
      }
      else{
        reject(e)
      }
    })
  }); 
}

Company.search = function(domain){
  var p = false;
  return new Promise(function(resolve, reject){
    searchWithMongo(domain).then(function(res){
      p = res;
    }).finally(function(){
      if(p){
        resolve(p);
      }
      else{
        resolve(searchWithClearbit(domain));
      }
    });
  });
}

Company.push = push;

module.exports = Company;
