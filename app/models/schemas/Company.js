'use strict';

var Mongoose = require('mongoose');

var CompanySchema = new Mongoose.Schema({
	id: {type: String, default: '', unique: true, index: true},
	name: {type: String, default: '', index: true},
	domain: {type: String, default: '', index: true, unique: true, lowercase: true},
	saveAt: {type: Number, default: 0}
});

module.exports = Mongoose.model('Company', CompanySchema);
