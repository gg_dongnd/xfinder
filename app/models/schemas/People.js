'use strict';

var Mongoose = require('mongoose');

var PeopleSchema = new Mongoose.Schema({
	id: {type: String, default: '', unique: true, index: true},
	name: {type: String, default: '', index: true},
	email: {type: String, default: '', index: true, unique: true, lowercase: true},
	gender: {type: String, default: ''},
	location: {type: String, default: ''},
	bio: {type: String, default: ''},
	site:	{type: String, default: ''},
	avatar:	{type: String, default: ''},
	twitter: {type: String, default: ''},
	linkedin: {type: String, default: ''},
	github: {type: String, default: ''},
	googleplus: {type: String, default: ''},
	facebook: {type: String, default: ''},
	angellist: {type: String, default: ''},
	gravatar: {type: String, default: ''},
	company: {type: Object, default: {}},
	saveAt: {type: Number, default: 0}
});

module.exports = Mongoose.model('People', PeopleSchema);
