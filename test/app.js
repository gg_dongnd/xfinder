/**
 * Testing
 * @ndaidong
 */
'use strict';
/*global describe, it, expect*/

//var Promise = require('bluebird');
//var Bella = require('bellajs');

var chai = require('chai');
//var should = chai.should();
var expect = chai.expect;
var supertest = require('supertest');

var app = require('../index.js');

var Feed = require('../app/models/Feed');

var config = app.get('config');

describe('Environment config', function(){

  it('should be an object', function(done){
    expect(config).to.be.a('object');
    done();
  });

  it('should include the main properties', function(done){
    expect(config).to.include.keys('name', 'version', 'application', 'port', 'mongodb');
    done();
  });
});

describe('Server Response', function(){

  describe('GET /', function() {
    it('should return 200 OK', function(done){
      supertest(app)
        .get('/')
        .expect(200, done);
    });
  });

  describe('GET /notfoundpage', function(){
    it('should return 404 OK', function(done){
      supertest(app)
        .get('/notfoundpage')
        .expect(404, done);
    });
  });
});


describe('Feed Model', function(){

  var feed = false;
  var skip = 0;
  var limit = 5;

  it('should return '+limit+' items', function(done){
    Feed.extract(skip, limit, function(res){
      if(res && res.entries && res.entries.length===limit){
        feed = res;
        done();
      }
    });
  });

  it('should contain all needed keys', function(done){
    expect(feed).to.include.keys('skip', 'limit', 'total', 'entries', 'paging');
    done();
  });

  it('should has correct skip and limit', function(done){
    expect(feed.skip).to.eql(skip);
    expect(feed.limit).to.eql(limit);
    done();
  });

  describe('GET /10000', function(){
    it('should return 404 OK', function(done){
      supertest(app)
        .get('/10000')
        .expect(404, done);
    });
  });
});
