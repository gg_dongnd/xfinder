/**
 * Common scenario for Gulp
 * @ndaidong at Twitter
 **/
'use strict';

var path = require('path');
var fs = require('fs');
var exec = require('child_process').execSync;

var bella = require('bellajs');
var mkdirp = require('mkdirp').sync;
var cpdir = require('copy-dir').sync;

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
///// Helpers //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

var clone = function(from, to, callback){
  var cb = callback || function(){};
  console.log('Clonning data from '+from+' to '+to);
  exec('git clone --depth=1 '+from+' '+to);
	console.log('Clonned data to '+to);
	exec('rm -rf '+to+'/.git');
	console.log('Removed .git folder from '+to+'/');
	cb();
}
var fixPath = function(p){
  return p?(p+=p.charAt(p.length-1)==='/'?'':'/'):p;
}

var helper = {
	createDir: function(ls){
		if(bella.isArray(ls)){
			var k = 0;
			ls.forEach(function(d){
				mkdirp(d);
				++k;
				console.log('%s, created dir "%s"... ', k, d);
			});
		}
		else{
			mkdirp(ls);
		}
		console.log('Done.');
	},
	removeDir: function(ls){
		if(bella.isArray(ls)){
			var k = 0;
			ls.forEach(function(d){
				exec('rm -rf '+d);
				++k;
				console.log('%s, removed dir "%s"... ', k, d);
			});
		}
		else{
			exec('rm -rf '+ls);
		}
		console.log('Done.');
	},
	publish: function(from, to){
		if(fs.existsSync(to)){
			helper.removeDir(to);
		}
		mkdirp(to);
		cpdir(from, to);
	},
	polymer: function(comp, to, callback){
		var cb = callback || function(){};
		var req = 'https://github.com/Polymer/';
		var filesToRemove = [
			'bower.json', 'demo.html', 'index.html', 'metadata.html', 'README.md'
		];
		var dirsToRemove = [
			'test'
		];
		var cleanit = function(p){
			console.log('Cleaning...');
			filesToRemove.forEach(function(f){
				var ff = p+'/'+f;
				if(fs.existsSync(ff)){
					fs.unlinkSync(ff);
					console.log('Removed '+ff);
				}
			});
			dirsToRemove.forEach(function(f){
				var ff = p+'/'+f;
				if(fs.existsSync(ff)){
					helper.removeDir(ff);
					console.log('Removed '+ff);
				}
			});
		}
		var from = req+comp+'.git';
		clone(from, to, function(){
			cleanit(to);
			cb();
		});
	},
	download: function(src, saveas, callback){

		var cb = callback || function(){};

		if(fs.existsSync(saveas)){
			fs.unlink(saveas);
		}
		console.log('Downloading '+src+'...');
		var cmd = 'wget -O '+saveas+' '+src;
		exec(cmd);
		console.log('Downloaded '+saveas);
		cb();
	},
	createEmptyFile: function(dest){
		var ext = path.extname(dest);
		var fname = path.basename(dest);
		var content = '';
		if(ext==='.js'){
			content = '/**'+fname+'*/';
		}
		else if(ext==='.css' || ext==='.less'){
			content = '/*'+fname+'*/';
		}
		fs.writeFileSync(dest, content, {encoding: 'utf8'});
	}
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

var pkg = require('./package'),
  bconf = pkg.builder || {},
  jsDir = fixPath(bconf.jsDir),
  imgDir = fixPath(bconf.imgDir),
  fontDir = fixPath(bconf.fontDir),
  distDir = fixPath(bconf.distDir);

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
///// Gulp tasks //////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

var gulp = require('gulp');

////////////////////////////////////////////////////////////////
gulp.task('dir', function(){
	var dirs = bconf.directories || [];
	if(bella.isArray(dirs) && dirs.length){
		helper.createDir(dirs);
	}
});

////////////////////////////////////////////////////////////////
gulp.task('file', function(){
	if(!fs.existsSync(jsDir)){
    mkdirp(jsDir);
  }
  var files = bconf.files || {};
	if(bella.isObject(files)){
		var destDir = jsDir;
		var rd = fixPath(destDir);
		for(var alias in files){
		var src = files[alias];
			var dest = rd+alias+'.js';
			if(!fs.existsSync(dest)){
				helper.download(src, dest);
			}
		}
	}
});

////////////////////////////////////////////////////////////////
gulp.task('img', function(){
	helper.publish(imgDir, distDir+'images/');
});

////////////////////////////////////////////////////////////////
gulp.task('font', function(){
	helper.publish(fontDir, distDir+'font/');
});

////////////////////////////////////////////////////////////////
gulp.task('reset', function(){
	var dirs = bconf.directories || [];
	if(bella.isArray(dirs) && dirs.length){
		dirs = bella.unique(dirs);
		helper.removeDir(dirs);
	}
	helper.removeDir(jsDir);
	helper.removeDir(distDir);
	helper.removeDir('node_modules');
});

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

gulp.task('setup', ['dir', 'img', 'font'], function(){

});
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

