# starter

Small Node.js based skeleton.

I created this because I need something basic and simple enough to start any  Node.js based web project, with the packages and technologies almost people familiar with, such as MongoDB, Promise, unitest, gzip, etc. 

This skeleton was built with Express, Mongoose, Bluebird, Async, Jade and Less. Using Mocha and Chai for test. Other plugins for auto compiling, compressing and merging client side resources for better performance.

```
npm start
npm test
```

How does it look like: http://techpush.net/

