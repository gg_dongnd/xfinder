'use strict';

var fs = require('fs');
var path = require('path');

var express = require('express');
var bodyParser = require('body-parser');
var compress = require('compression');
var connectAssets = require('connect-assets');
var favicon = require('serve-favicon');
var helmet = require('helmet');

var mongoose = require('mongoose');

var bella = require('bellajs');

var app = express();

var env = app.get('env') || 'development';

var configAll = require('./app/config/base');
var configEnv = require('./app/config/env.conf');
var config = bella.copies(configEnv, configAll);
config.ENV = env;

app.set('config', config);

/**
 * Connect to MongoDB.
 */
if(config.mongodb){
  mongoose.connect(config.mongodb.url);
  mongoose.connection.on('error', function(){
    console.error('MongoDB Connection Error. Please make sure that MongoDB is running.');
  });
}

/**
 * Express configuration.
 */
app.set('port', config.port);
app.set('views', path.join(__dirname, 'app/views'));
app.set('view engine', 'jade');
app.set('etag', 'strong');

app.use(compress());
app.use(helmet());

app.use(helmet.xssFilter());
app.use(helmet.frameguard());
app.use(helmet.ieNoOpen());
app.use(helmet.noSniff());
app.use(helmet.hidePoweredBy({setTo: config.application.name}));

app.use(connectAssets({
  build: {dev: false, prov: true},
  compress: {dev: false, prov: true},
  fingerprinting: {dev: false, prov: true},
  gzip: true,
  buildDir: 'dist/assets/',
  paths: [path.join(__dirname, './assets/css'), path.join(__dirname, './assets/js')],
  servePath: 'assets'
}));

app.use(favicon(path.join(__dirname, '/assets/images')+'/brand/favicon.ico'));
app.use(express.static(path.join(__dirname, 'dist'), {maxAge: 3*24*60*6e4, etag: true, lastModified: true}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(function(req, res, next){
  res.config = config;
  var ua = req.headers['user-agent'];
  if(ua){
    var di = bella.detectDevice(ua);
    res.device = di;
  }
  res.render404 = function(){
    res.status(404);
    var pat = (function(p){
      var ps = [];
      if(!!p && p.length>0){
        var _ps = p.charAt(0)=='/'?p.slice(1):p;
        if(!!_ps && _ps.length>0){
          ps = _ps.split('/');
        }
      }
      return ps;
    })(req.path || '');

    var msg = 'The URL you request "/'+pat.join('/')+'" was not found!';
    res.render('errors/404.jade', {
      title: '404: Not Found',
      message: msg
    });
  }
  next();
});

app.use(function(req, res, next){
  var render = res.render;
  res.render = function(view, locals, cb){
    if(bella.isObject(locals)){
      if(!locals.meta){
        locals.meta = config.application;
      }
    }
    render.call(res, view, locals, cb);
  }
  next();
});

app.locals.ucfirst = function(value){
  if(bella.isString(value) && value.length>1){
    return value.charAt(0).toUpperCase() + value.slice(1);
  }
  return '';
}

fs.readdirSync('./app/routers').forEach(function(file){
  if(path.extname(file)==='.js'){
    require('./app/routers/' +file)(app);
  }
});

// Handle 404
app.use(function(req, res){
  res.render404();
});

// Handle 500
app.use(function(error, req, res){
  res.status(500);
  res.render('errors/500.jade', {
    title: '500: Internal Server Error',
    error: error
  });
});

/**
 * Start Express server.
 */
function onServerReady(){
  require('./app/workers/builder').setup();
}

app.listen(config.port, function(){
  console.log('Express server listening on port %d in %s mode', config.port, config.ENV);
  console.log('http://127.0.0.1:'+config.port);
  onServerReady();
});

module.exports = app;
