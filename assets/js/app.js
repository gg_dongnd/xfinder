// main.js
/*globals requirejs, async, angular, Bella, qwest, $, jsLoaderConf, twttr */

;(function(){

    'use strict';

    var app = window.app || {}

    app.init = function(){
      onPageLoaded();
    }

    function onPageLoaded(){
      Bella.dom.all('span.date-time').forEach(function(el){
        var t = el.getAttribute('datetime')*1;
        el.html(Bella.date.relativize(t));
        el.setAttribute('title', Bella.date.format(false, t));
      });
    }

    app.init();
})();
