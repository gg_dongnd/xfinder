/*globals requirejs, angular, Bella, $, jsLoaderConf, twttr */

;(function(){

    'use strict';

    var app = window.app || {};

    function fixThumbImages(){
        Bella.dom.all('.link-no-bg').forEach(function(el){
           var aid = el.getAttribute('rel');
           console.log(aid);
           $.get('/pictures/take/'+aid)
             .then(function(res){
                var img = '/images/bg/file-not-found.png';
                if(res.result && res.result.image){
                    img = res.result.image;
                }
                el.setAttribute('style', 'background-image: url('+img+')');
             });
        });
    }

    function handleScroll(){

    }

    app.fixThumbImages = fixThumbImages;
    app.handleScroll = handleScroll;
})();
